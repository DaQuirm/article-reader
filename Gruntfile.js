var path = require('path');

module.exports = function(grunt) {

	grunt.loadNpmTasks('grunt-contrib-clean');
	grunt.loadNpmTasks('grunt-contrib-coffee');
	grunt.loadNpmTasks('grunt-contrib-compress');
	grunt.loadNpmTasks('grunt-contrib-copy');
	grunt.loadNpmTasks('grunt-contrib-jade');
	grunt.loadNpmTasks('grunt-contrib-stylus');

	grunt.initConfig({

		clean: ['build/*'],

		coffee: {
			compile: {
				options: {
					bare: true
				},
				files: {
					'build/reader.js': ['src/*.coffee']
				}
			}
		},

		compress: {
			main: {
				options: {
					archive: 'build/reader.nw',
					mode: 'zip'
				},
				files: [
					{ src: '**/*', cwd:'build/', expand: true }
				]
			}
		},

		copy: {
			main: {
				files: [
					/* Libraries */
					{ src: 'lib/jade-runtime/jade.runtime.js', dest: 'build/jade.runtime.js' },
					{ src: 'lib/moment/moment.js', dest: 'build/moment.js' },
					{ src: 'node_modules/adm-zip/**/*', dest: 'build/' },
					/* Source */
					{ src: 'src/index.html', dest: 'build/index.html' },
					{ src: 'src/package.json', dest: 'build/package.json' },
					/* Fonts */
					{ src: 'src/fonts/open-sans/OpenSans-Italic.ttf', dest: 'build/fonts/OpenSans-Italic.ttf' },
					{ src: 'src/fonts/open-sans/OpenSans-Light.ttf', dest: 'build/fonts/OpenSans-Light.ttf' },
					{ src: 'src/fonts/open-sans/OpenSans-LightItalic.ttf', dest: 'build/fonts/OpenSans-LightItalic.ttf' },
					{ src: 'src/fonts/open-sans/OpenSans-Regular.ttf', dest: 'build/fonts/OpenSans-Regular.ttf' },
					{ src: 'src/fonts/open-sans/OpenSans-Semibold.ttf', dest: 'build/fonts/OpenSans-Semibold.ttf' },
					{ src: 'src/fonts/open-sans/OpenSans-SemiboldItalic.ttf', dest: 'build/fonts/OpenSans-SemiboldItalic.ttf' }
				]
			}
		},

		jade: {
			compile: {
				options: {
					compileDebug: false,
					client: true,
					namespace: 'Reader.Templates',
					processName: function(filename) {
						var basename = path.basename(filename, '.jade');
						return basename.charAt(0).toUpperCase() + basename.slice(1);
					}
				},
				files: {
					'build/templates.js': 'src/templates/*.jade'
				}
			}
		},

		stylus: {
			compile: {
				files: {
					'build/style.css': 'src/styles/style.styl'
				}
			}
		}

	});

	grunt.registerTask('build', [
		'clean',
		'copy',
		'coffee',
		'jade',
		'stylus',
		'compress'
	]);

	grunt.registerTask('default', [ 'build' ]);
};
