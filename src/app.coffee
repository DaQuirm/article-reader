window.Reader or= {}

AdmZip = require 'adm-zip'

Reader.App = class Reader.App

	constructor: (@container)->

	run: ->
		html = do Reader.Templates.Reader
		@container.innerHTML = html

		dropArea = document.querySelector '.drop-area'

		dropArea.addEventListener 'dragenter', (event) =>
			do event.stopPropagation
			do event.preventDefault
			dropArea.classList.add 'active'

		dropArea.addEventListener 'dragleave', (event) =>
			do event.stopPropagation
			do event.preventDefault
			dropArea.classList.remove 'active'

		dropArea.addEventListener 'dragover', (event) =>
			do event.stopPropagation
			do event.preventDefault

		dropArea.addEventListener 'drop', (event) =>
			do event.stopPropagation
			do event.preventDefault
			files = event.dataTransfer.files
			if files.length > 0
				file = files[0]
				@unzip file.path
				do @render

	unzip: (path) ->
		zip = new AdmZip path
		zipEntries = do zip.getEntries
		@files = {}
		zipEntries.forEach (zipEntry) =>
			if zipEntry.entryName is "article.json"
				@article = JSON.parse(zip.readAsText zipEntry)
			@files[zipEntry.entryName] = zip.readFile zipEntry

	render: ->
		document.title = ".article reader - #{@article.resources.info.title}"
		@article.resources.info.formatted_date = moment(new Date @article.resources.info.date).format 'MMMM Do YYYY'
		@article.resources.images.forEach (image) =>
			image.base64 = @files[image.file].toString 'base64'
		html = Reader.Templates.Article @article
		@container.innerHTML = html
